module.exports = {
  env: {
    browser: true,
    node: true,
  },
  extends: "eslint:recommended",
  parserOptions: {
    ecmaVersion: 6,
  },
  globals: {
    /* MOCHA */
    describe: false,
    it: false,
    beforeAll: false,
    before: false,
    console: false,
    Promise: false,
  },
  rules: {
    "no-console": 0,
    indent: ["error", 2],
    "linebreak-style": ["error", "unix"],
    // "quotes": [
    //   "error",
    //   "double"
    // ],
    semi: ["error", "never"],
  },
}
