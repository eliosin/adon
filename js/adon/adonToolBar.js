jQuery.fn.extend({
  adonToolBar: function (fa_icon, starts_active) {
    return $(this).each(function () {
      var tag = $(this)
      var tag_name = tag.prop("tagName")

      tag.addClass("adonToolSlave")

      var TAGBAR = $("<" + tag_name + "></" + tag_name + ">") // Create with jQuery
      TAGBAR.addClass("adonToolBar")
      $(this).before(TAGBAR)

      var TAGBARICON = $("<i class='fas'></i>")
      TAGBARICON.addClass(fa_icon)
      TAGBAR.append(TAGBARICON)

      tag.addClass("slavehidden")
      TAGBAR.addClass("slavehidden")
      TAGBAR.click(function () {
        tag.toggleClass("slavehidden")
        TAGBAR.toggleClass("slavehidden")
      })
      // console.log(tag.prop('tagName') + " addClass adonToolBar > " + fa_icon);
      return TAGBAR
    })
  },
  _adofToolBar: function () {
    return $(this).each(function () {
      var tag = $(this)
      tag.removeClass("adonToolSlave")
      TAGBAR = tag.before()
      TAGBAR.remove()
      // console.log(tag.prop('tagName') + " removeClass adonToolSlave");
      return tag
    })
  },
})

// and it ends!
