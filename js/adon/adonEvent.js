// Usage
// $(document).ready(function() {
//   $('h1').adonClick("adonBulgeText")
// })
jQuery.fn.extend({
  adonClick: function (adonName) {
    return $(this).each(function () {
      var tag = $(this)
      tag.click(function () {
        tag.toggleClass(adonName)
      })
    })
  },
})

// Usage
// $(document).ready(function() {
//   $('h1').adonDoubleClick("adonBulgeText")
// })
jQuery.fn.extend({
  adonDoubleClick: function (adonName) {
    return $(this).each(function () {
      var tag = $(this)
      tag.dblclick(function () {
        tag.toggleClass(adonName)
      })
    })
  },
})

// Usage
// $(document).ready(function() {
//   $('h1').adonHover("adonBulgeText")
// })
jQuery.fn.extend({
  adonHover: function (adonName) {
    return $(this).each(function () {
      var tag = $(this)
      tag.hover(function () {
        tag.toggleClass(adonName)
      })
    })
  },
})

// Usage
// $(document).ready(function() {
//   $('h1').adonFocus("adonBulgeText")
// })
jQuery.fn.extend({
  adonFocus: function (adonName) {
    return $(this).each(function () {
      var tag = $(this)
      tag.focus(function () {
        tag.toggleClass(adonName)
      })
    })
  },
})
