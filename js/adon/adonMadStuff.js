var tid = 0
jQuery.fn.extend({
  adonMadStuff: function (goMad) {
    return $(this).each(function () {
      var tag = $(this)

      $(this).addClass("adonMadStuffCol")
      if (goMad) tag.addClass("adonMadStuff")

      tid = setInterval(function () {
        tag._madStuff(tag)
      }, 6666)

      // console.log(tag.prop('tagName') + " addClass adonMadStuff");
      return tag
    })
  },
  _adofMadStuff: function () {
    return $(this).each(function () {
      var tag = $(this)

      tag.removeClass("adonMadStuffCol")
      tag.removeClass("adonMadStuff")

      clearInterval(tid)

      // console.log(tag.prop('tagName') + " removeClass adonMadStuff");
      return $(this)
    })
  },
  _madStuff: function (tag) {
    tag.toggleClass("madactive")
    // console.log(tag.prop('tagName') + " adonactive");
  },
})
