const suites = require("../suites/adonTesterSuite")

var testTag = "h1"
var testML = `<${testTag}>PRETEST</${testTag}>`

suites.AdonTesterSuite(
  "jQuery.fn.extend.adonEvent",
  "../../js/adon/adonEvent.js",
  testML,
  function () {
    it("adonClick", done => {
      $(testTag)[0].should.not.have.class("adonTestClass")
      $(testTag).adonClick("adonTestClass")
      $(testTag)[0].should.not.have.class("adonTestClass") // until clicked
      $(testTag)[0].click()
      $(testTag)[0].should.have.class("adonTestClass") // when clicked
      $(testTag)[0].click()
      $(testTag)[0].should.not.have.class("adonTestClass") // toggled when clicked
      done()
    })
  }
)
