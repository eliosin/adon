const suites = require("../suites/adonTesterSuite")

var testTag = "h1"
var testML = `<${testTag}>PRETEST</${testTag}>`

suites.AdonTesterSuite(
  "jQuery.fn.extend.adonHelloWorld",
  "../../js/adon/adonHelloWorld.js",
  testML,
  function () {
    it("the DOM has some test html", done => {
      // $("body").innerHTML = "<blockquote>PRETEST</blockquote>";
      $(testTag)[0].should.have.text("PRETEST")
      $(testTag)[0].should.not.have.class("adonHelloWorld")
      done()
    })

    it("adds class adonHelloWorld to tag", done => {
      $(testTag)[0].should.have.text("PRETEST")
      $(testTag)[0].should.not.have.class("adonHelloWorld")
      $(testTag).adonHelloWorld()
      $(testTag)[0].should.have.text("Hello World")
      $(testTag)[0].should.have.class("adonHelloWorld")
      done()
    })

    it("puts back the original text", done => {
      $(testTag)[0].should.have.text("PRETEST")
      $(testTag).adonHelloWorld()
      $(testTag)[0].should.have.text("Hello World")
      $(testTag)._goodbyeWorld()
      $(testTag)[0].should.have.text("PRETEST")
      done()
    })

    it("removes class adonHelloWorld from element", done => {
      $(testTag).adonHelloWorld()
      $(testTag)[0].should.have.class("adonHelloWorld")
      $(testTag)._goodbyeWorld()
      $(testTag)[0].should.not.have.class("adonHelloWorld")
      done()
    })
  }
)
