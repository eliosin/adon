const suites = require("../suites/adonTesterSuite")
global.expect = require("chai").use(require("chai-dom")).expect

var testTag = "h1"
var testML = `<${testTag}>PRETEST</${testTag}>`

suites.AdonTesterSuite(
  "jQuery.fn.extend.adonToolBar",
  "../../js/adon/adonToolBar.js",
  testML,
  function () {
    it("setup and tear down", done => {
      $(testTag).adonToolBar("fa-fire", true)
      expect($(".adonToolSlave").length).to.equal(1)
      expect($(".adonToolBar").length).to.equal(1)
      expect($(testTag).length).to.equal(2)
      $(".adonToolBar")[0].should.have.class("slavehidden")
      $(".adonToolSlave")[0].should.have.class("slavehidden")
      done()
    })
    it("test click event", done => {
      $(testTag).adonToolBar("fa-fire", true)
      $(".adonToolBar")[0].click()
      $(".adonToolBar")[0].should.not.have.class("slavehidden")
      $(".adonToolSlave")[0].should.not.have.class("slavehidden")
      $(".adonToolBar")[0].click()
      $(".adonToolBar")[0].should.have.class("slavehidden")
      $(".adonToolSlave")[0].should.have.class("slavehidden")
      done()
    })
  }
)
