<aside>
  <dl>
  <dd>Wherein all things created first he weighed,</dd>
  <dd>The pendulous round earth with balanced air</dd>
  <dd>In counterpoise, now ponders all events</dd>
</dl>
</aside>

**adon** extends **god** with some development _patterns_ for building jQuery prototypes which expect pronged tagNames as selectors.

With **god** taking care of the layout, by ensuring all your tags are laid out splendidly, you can use the _patterns_ established in **adon** to style them in reaction to browser events. **adon** is there to answer question: What happens if I click this?

For instance, you might put the `nav` tag in _hell_ which, if you think about it, is a perfectly logically place. You might want the `nav` tag to be sticky and show as a simple button, stuck against the left side of the button. When the user clicks the button, you want the contents of the `nav` tag to show.

Since **god** must take of the `nav`s position, and **eve** its styling, all **adon** need do is execute an `$('nav>ul').addStyle('visibility:visible');` when the `nav` is clicked.

But sometimes you might want something more complicated than that. **adon**'s main job is to decide when and how to add and remove special className(s). These className must be unique to the adon and its CSS style for it.

**god** doesn't recognise classNames, and **eve** only in special circumstances, so the style for **adon**'s classNames remains its control. **adon** likes classNames, but only its _own_ classNames, which it can target to change the style triggered by user interaction.

<div>
  <a href="demo.html" target="_demo">
  <button>Demo</button>
</a>
</div>

# Nutshell

`adons` - in principle - work in elioWays:

- **adon**'s extend jQuery.
- tagNames are selectors, not classNames.
- Each **adon** gets its own js file so it can be imported singularly.
- The **adon** sets one or more classNames on the tag.
- The **adon** gets a scss file to style the adon's className. _optional_
- adons are uglified to am adon.js file.
- adons are initialized in `main.js`.
- `adon.scss` is referenced by `theme.scss` (or `judge.scss` if you don't want it in your theme).

# A cult, not a framework

1. Only **adon** shall be responsible for jQuery style interactions with tagNames.
2. **adon**'s shall use classNames to apply the styling.

_Terms and conditions apply. The **adon** retains the right to change these rules at a whim._
