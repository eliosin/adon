# adon Credits

## Core! thanks

- [svgimages:leaf](https://www.svgimages.com/leaf.html#size=512)
- [pixabay:hell-purgatory-heaven-stairs-path](https://pixabay.com/en/hell-purgatory-heaven-stairs-path-735995/)
- [wikimedia:Adam%27s_Apple](https://commons.wikimedia.org/wiki/File:Adam%27s_Apple_-_panoramio.jpg)
- [wikimedia:Jamnik,_Kranj_-\_The_road_to_heaven](https://commons.wikimedia.org/wiki/File:Jamnik,_Kranj_-_The_road_to_heaven.jpg)

## Useful for testing

- [mocha-jsdom](https://github.com/rstacruz/mocha-jsdom)
