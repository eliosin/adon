![](https://elioway.gitlab.io/eliosin/adon/elio-adon-logo.png)

> What happens if I click this? **the elioWay**

# adon ![alpha](https://elioway.gitlab.io/eliosin/icon/devops/alpha/favicon.ico "alpha")

The place to find jQuery _patterns_ in **the elioWay**, **adon** works with [god](https://elioway.gitlab.io/eliosin/god) with working examples so you can create your own eliosin.adon()

- [adon Documentation](https://elioway.gitlab.io/eliosin/adon)
- [adon Demo](https://elioway.gitlab.io/eliosin/adon/demo.html)

## Installing

```shell
npm install @elioway/adon --save
yarn add  @elioway/adon --dev
```

- [Installing adon](https://elioway.gitlab.io/eliosin/adon/installing.html)

## Seeing is Believing

```shell
git clone https://gitlab.com/eliosin/adon/
cd adon
npm i|yarn
gulp
```

## Nutshell

### `gulp`

### `npm run test`

### `npm run prettier`

- [adon Quickstart](https://elioway.gitlab.io/eliosin/adon/quickstart.html)
- [adon Credits](https://elioway.gitlab.io/eliosin/adon/credits.html)

![](https://elioway.gitlab.io/eliosin/adon/apple-touch-icon.png)

## License

[HTML5 Boilerplate](LICENSE.txt) [Tim Bushell](mailto:theElioWay@gmail.com)
